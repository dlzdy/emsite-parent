/**
 * Copyright &copy; 2017 <a href="https://gitee.com/hackempire/emsite-parent">emsite</a> All rights reserved.
 */
package com.empire.emsite.test.facade;

import com.empire.emsite.common.persistence.Page;
import com.empire.emsite.test.dto.TestDataDTO;

/**
 * 类TestDataFacadeService.java的实现描述：单表生成FacadeService接口
 * 
 * @author arron 2017年9月17日 下午9:56:20
 */
public interface TestDataFacadeService {

    /* public List<TestData> findList(TestData testData); */

    public Page<TestDataDTO> findPage(Page<TestDataDTO> page, TestDataDTO testDataDTO);

    public TestDataDTO get(String id);

    public void save(TestDataDTO testDataDTO);

    public void delete(TestDataDTO testDataDTO);

}
