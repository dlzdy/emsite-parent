/**
 * Copyright &copy; 2017 <a href="https://gitee.com/hackempire/emsite-parent">emsite</a> All rights reserved.
 */
package com.empire.emsite.modules.cms.facade.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.empire.emsite.modules.cms.entity.ArticleData;
import com.empire.emsite.modules.cms.facade.ArticleDataFacadeService;
import com.empire.emsite.modules.cms.service.ArticleDataService;

/**
 * 类ArticleDataFacadeServiceImpl.java的实现描述：站点FacadeService接口实现类
 * 
 * @author arron 2017年9月17日 下午9:44:44
 */
@Service
public class ArticleDataFacadeServiceImpl implements ArticleDataFacadeService {
    @Autowired
    private ArticleDataService articleDataService;

    @Override
    public ArticleData get(String id) {
        return articleDataService.get(id);
    }
}
