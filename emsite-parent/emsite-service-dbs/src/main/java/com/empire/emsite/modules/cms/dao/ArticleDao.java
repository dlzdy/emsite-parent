/**
 * Copyright &copy; 2017 <a href="https://gitee.com/hackempire/emsite-parent">emsite</a> All rights reserved.
 */
package com.empire.emsite.modules.cms.dao;

import java.util.List;

import com.empire.emsite.common.persistence.CrudDao;
import com.empire.emsite.common.persistence.annotation.MyBatisDao;
import com.empire.emsite.modules.cms.entity.Article;
import com.empire.emsite.modules.cms.entity.Category;

/**
 * 类ArticleDao.java的实现描述：文章DAO接口
 * 
 * @author arron 2017年10月30日 下午4:07:04
 */
@MyBatisDao
public interface ArticleDao extends CrudDao<Article> {

    public List<Article> findByIdIn(String[] ids);

    //	{
    //		return find("from Article where id in (:p1)", new Parameter(new Object[]{ids}));
    //	}

    public int updateHitsAddOne(String id);

    //	{
    //		return update("update Article set hits=hits+1 where id = :p1", new Parameter(id));
    //	}

    public int updateExpiredWeight(Article article);

    public List<Category> findStats(Category category);
    //	{
    //		return update("update Article set weight=0 where weight > 0 and weightDate < current_timestamp()");
    //	}

}
