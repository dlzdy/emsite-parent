/**
 * Copyright &copy; 2017 <a href="https://gitee.com/hackempire/emsite-parent">emsite</a> All rights reserved.
 */
package com.empire.emsite.modules.cms.web;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.apache.shiro.authz.annotation.RequiresUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.empire.emsite.common.config.MainConfManager;
import com.empire.emsite.common.utils.StringUtils;
import com.empire.emsite.common.web.BaseController;
import com.empire.emsite.modules.cms.entity.Article;
import com.empire.emsite.modules.cms.entity.Category;
import com.empire.emsite.modules.cms.entity.Site;
import com.empire.emsite.modules.cms.facade.CategoryFacadeService;
import com.empire.emsite.modules.cms.facade.SiteFacadeService;
import com.empire.emsite.modules.cms.service.FileTplService;
import com.empire.emsite.modules.cms.utils.CmsUtils;
import com.empire.emsite.modules.cms.utils.TplUtils;
import com.empire.emsite.modules.sys.utils.UserUtils;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

/**
 * 类CategoryController.java的实现描述：栏目Controller
 * 
 * @author arron 2017年10月30日 下午7:10:41
 */
@Controller
@RequestMapping(value = "${adminPath}/cms/category")
public class CategoryController extends BaseController {

    @Autowired
    private CategoryFacadeService categoryFacadeService;
    @Autowired
    private FileTplService        fileTplService;
    @Autowired
    private SiteFacadeService     siteFacadeService;

    @ModelAttribute("category")
    public Category get(@RequestParam(required = false) String id) {
        if (StringUtils.isNotBlank(id)) {
            return categoryFacadeService.get(id);
        } else {
            return new Category();
        }
    }

    @RequiresPermissions("cms:category:view")
    @RequestMapping(value = { "list", "" })
    public String list(Model model) {
        List<Category> list = Lists.newArrayList();
        List<Category> sourcelist = categoryFacadeService.findByUser(true, null, UserUtils.getUser(),
                CmsUtils.getCurrentSiteId());
        Category.sortList(list, sourcelist, "1");
        model.addAttribute("list", list);
        return "modules/cms/categoryList";
    }

    @RequiresPermissions("cms:category:view")
    @RequestMapping(value = "form")
    public String form(Category category, Model model) {
        if (category.getParent() == null || category.getParent().getId() == null) {
            category.setParent(new Category("1"));
        }
        Category parent = categoryFacadeService.get(category.getParent().getId());
        category.setParent(parent);
        if (category.getOffice() == null || category.getOffice().getId() == null) {
            category.setOffice(parent.getOffice());
        }
        model.addAttribute("listViewList", getTplContent(Category.DEFAULT_TEMPLATE));
        model.addAttribute("category_DEFAULT_TEMPLATE", Category.DEFAULT_TEMPLATE);
        model.addAttribute("contentViewList", getTplContent(Article.DEFAULT_TEMPLATE));
        model.addAttribute("article_DEFAULT_TEMPLATE", Article.DEFAULT_TEMPLATE);
        model.addAttribute("office", category.getOffice());
        model.addAttribute("category", category);
        return "modules/cms/categoryForm";
    }

    @RequiresPermissions("cms:category:edit")
    @RequestMapping(value = "save")
    public String save(Category category, Model model, RedirectAttributes redirectAttributes) {
        if (MainConfManager.isDemoMode()) {
            addMessage(redirectAttributes, "演示模式，不允许操作！");
            return "redirect:" + adminPath + "/cms/category/";
        }
        if (!beanValidator(model, category)) {
            return form(category, model);
        }
        category.setCurrentUser(UserUtils.getUser());
        category.setSite(new Site(CmsUtils.getCurrentSiteId()));
        categoryFacadeService.save(category);
        UserUtils.removeCache(UserUtils.CACHE_CATEGORY_LIST);
        CmsUtils.removeCache("mainNavList_" + category.getSite().getId());
        addMessage(redirectAttributes, "保存栏目'" + category.getName() + "'成功");
        return "redirect:" + adminPath + "/cms/category/";
    }

    @RequiresPermissions("cms:category:edit")
    @RequestMapping(value = "delete")
    public String delete(Category category, RedirectAttributes redirectAttributes) {
        if (MainConfManager.isDemoMode()) {
            addMessage(redirectAttributes, "演示模式，不允许操作！");
            return "redirect:" + adminPath + "/cms/category/";
        }
        if (Category.isRoot(category.getId())) {
            addMessage(redirectAttributes, "删除栏目失败, 不允许删除顶级栏目或编号为空");
        } else {
            categoryFacadeService.delete(category);
            UserUtils.removeCache(UserUtils.CACHE_CATEGORY_LIST);
            CmsUtils.removeCache("mainNavList_" + category.getSite().getId());
            addMessage(redirectAttributes, "删除栏目成功");
        }
        return "redirect:" + adminPath + "/cms/category/";
    }

    /**
     * 批量修改栏目排序
     */
    @RequiresPermissions("cms:category:edit")
    @RequestMapping(value = "updateSort")
    public String updateSort(String[] ids, Integer[] sorts, RedirectAttributes redirectAttributes) {
        int len = ids.length;
        Category[] entitys = new Category[len];
        for (int i = 0; i < len; i++) {
            entitys[i] = categoryFacadeService.get(ids[i]);
            entitys[i].setSort(sorts[i]);
            entitys[i].setCurrentUser(UserUtils.getUser());
            categoryFacadeService.save(entitys[i]);
            UserUtils.removeCache(UserUtils.CACHE_CATEGORY_LIST);
            CmsUtils.removeCache("mainNavList_" + entitys[i].getSite().getId());
        }
        addMessage(redirectAttributes, "保存栏目排序成功!");
        return "redirect:" + adminPath + "/cms/category/";
    }

    @RequiresUser
    @ResponseBody
    @RequestMapping(value = "treeData")
    public List<Map<String, Object>> treeData(String module, @RequestParam(required = false) String extId,
                                              HttpServletResponse response) {
        response.setContentType("application/json; charset=UTF-8");
        List<Map<String, Object>> mapList = Lists.newArrayList();
        List<Category> list = categoryFacadeService.findByUser(true, module, UserUtils.getUser(),
                CmsUtils.getCurrentSiteId());
        for (int i = 0; i < list.size(); i++) {
            Category e = list.get(i);
            if (extId == null
                    || (extId != null && !extId.equals(e.getId()) && e.getParentIds().indexOf("," + extId + ",") == -1)) {
                Map<String, Object> map = Maps.newHashMap();
                map.put("id", e.getId());
                map.put("pId", e.getParent() != null ? e.getParent().getId() : 0);
                map.put("name", e.getName());
                map.put("module", e.getModule());
                mapList.add(map);
            }
        }
        return mapList;
    }

    private List<String> getTplContent(String prefix) {
        List<String> tplList = fileTplService.getNameListByPrefix(siteFacadeService.get(CmsUtils.getCurrentSiteId())
                .getSolutionPath());
        tplList = TplUtils.tplTrim(tplList, prefix, "");
        return tplList;
    }
}
