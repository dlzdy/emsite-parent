/**
 * Copyright &copy; 2017 <a href="https://gitee.com/hackempire/emsite-parent">emsite</a> All rights reserved.
 */
package com.empire.emsite.common.beanvalidator;

/**
 * 类DefaultGroup.java的实现描述：默认Bean验证组
 * 
 * @author arron 2017年10月30日 下午6:28:37
 */
public interface DefaultGroup {

}
